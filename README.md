# Interactive Programming

Le but de ce dépôt est de prendre l'habitude de pratiquer "l'interactive programming"
avec Scheme.

Le fichier shell.nix contient :
  - Chez Scheme
  - Emacs
  - Geiser

Sur Debian, installer `emacs`, `chezscheme`, `elpa-geiser`, et `racket`.

Pour commencer rapidement :

```
emacs -nw album.ss
```

Ensuite quelques raccourcis pratiques :

```
C-x C-s - Sauvegarder
C-x k - Fermer le buffer
C-c C-c - Evaluer la définition sous le curseur
C-c C-z - Ouvrir le REPL
```
