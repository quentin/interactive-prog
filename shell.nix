{ pkgs ? import <nixpkgs> {} }: 

pkgs.mkShell {
  nativeBuildInputs = [
    ((pkgs.emacsPackagesFor pkgs.emacs).emacsWithPackages
      (epkgs: [ pkgs.emacs28Packages.geiser-chez ]))
    pkgs.chez
  ];
}
